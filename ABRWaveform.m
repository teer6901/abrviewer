classdef ABRWaveform < handle
    
    properties
        time double
        ABR double
        parameter double
        label string
        id uint32
        maxAmp double
    end
    
    properties (Access = private)
        parent ABRWaveformCollection
        lineHandle matlab.graphics.primitive.Line
        markerHandles matlab.graphics.primitive.Line
        yOffset double
        color 
    end
    
    methods
        
        function obj = ABRWaveform(varargin)
            p = inputParser;
            p.addParameter('parent', obj.parent);
            p.addParameter('time', obj.time);
            p.addParameter('ABR', obj.ABR);
            p.addParameter('parameter', obj.parameter);
            p.addParameter('label', obj.label);
            p.parse(varargin{:});
            
            obj.parent = p.Results.parent;
            obj.time = p.Results.time;
            obj.ABR = p.Results.ABR;
            obj.maxAmp = max(abs(obj.ABR));
            obj.parameter = p.Results.parameter;
            obj.label = p.Results.label;
            obj.color = hsv2rgb(rand(1), (1+rand(1))/2, (1+rand(1))/2);
            obj.yOffset = 0;
        end
        
        function delete(obj)
            delete(obj.lineHandle);
            delete(obj.markerHandles);
            if ~isempty(obj.parent) && isvalid(obj.parent)
                obj.parent.removeWaveform(obj);
            end
        end
        
        function iseq = eq(A, B)
            if isempty(A) || isempty(B)
                iseq = logical([]);
            else
                iseq = ...
                    ~isempty([A.parameter]) & ~isempty([B.parameter]) ...
                    & ~isempty([A.label]) & ~isempty([B.label]) ...
                    & [A.parameter] == [B.parameter] ...
                    & [A.label] == [B.label];
                iseq = iseq(:);
            end
        end
        
    end
    
    methods (Access = public)
        
        function setParent(obj, parent)
            obj.parent = parent;
        end

        function mx = setOffset(obj, offset)
            obj.yOffset = offset;
            mx = obj.maxAmp;
        end
        
        function mx = setColor(obj, color)
            obj.color = color;
        end
        
        function updateGraph(obj)
            hax = obj.parent.getAxes;
            if isempty(obj.lineHandle) || ~ishandle(obj.lineHandle)
                obj.createGraph(hax);
            end
            set(obj.lineHandle, ...
                'XData', obj.time/1e-3, ...
                'YData', obj.ABR + obj.yOffset, ...
                'Color', obj.color);
        end
        
        function createGraph(obj, hax)
            obj.lineHandle = line(hax);
        end
        
        function switchGraph(obj, state)
            if state 
                set(obj.lineHandle, 'Visible', 'on');
            else
                set(obj.lineHandle, 'Visible', 'off');
            end
        end
        
    end
    
end

