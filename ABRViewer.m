function ABRViewer
%ABRVIEWER  ABR waveform viewer and analysis tool
%
%
% Copyright 2021 Rainer Beutelmann, Universität Oldenburg
% ABRViewer by Rainer Beutelmann (Universität Oldenburg) is licensed under CC BY-SA 4.0
%


feature('DefaultCharacterSet','UTF-8');
abr_list = ABRViewerList;

